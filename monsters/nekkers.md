<img src='https://i.imgur.com/ZFV3CP7.png' />

<!-- style='top:50px;width:280px;height:380px;' -->

## Nekkers
Awful stench and gore are usually found before a Nekker rends an adventurer with their claws. Distant and mutated cousins of giants, these small creatures dwell in caves near roads. Waylaying travelers and merchants alike. Relieiving people of their flesh is a Nekker's favorite pasttime.

Imagine a drowner that burrows tunnels, climbs trees, is more vicious than usual, and when ambushing its prey, it does so with many of its kin. Now you have a good idea of what a nekker is. These primitive creatures are the bane of the wilderness - the inhabitants of forest villages fear them, and animals give their nests a wide berth. Nekkers are social creatures, gathering in something akin to tribes, for they can only repel the attacks of stronger assailants en masse.


___
> ## Nekker
>*Small Giant, chaotic evil*
 ___

> - **Hit Points** 8 (1d12 + 1)
> - **Speed** 30ft.,  burrow 20ft
> - **Initiative** +1
> - **Armor Class** 14 (natural armor)
> - **Base Attack/Grapple** +1/+3
> - **Attack** Claw +4 to hit (1d6 + 2)
> - **Full Attack** Claw +4 to hit (1d6 + 2)
> - **Space/Reach** 5 ft./5 ft.
> - **Special Attacks** -
> - **Special Qualities** Darkvision 60 ft.
> - **Saves** Fort +3, Ref +1, Will +0
> - **Languages** Giant

___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|14 (+2)|12 (+1)|12 (+1)|8 (-1)|10 (+0)|6 (-2)|

> - **Challenge Rating** 1/2
___

```
```
___
> ## Nekker Warrior
>*Small Giant, chaotic evil*
>___

> - **Hit Points** 43 (6d12 + 1)
> - **Speed** 30ft.,  burrow 20ft
> - **Initiative** +2
> - **Armor Class** 16 (natural armor)
> - **Skills** Hide +6, Listen +4, Move Silently +7, Ride +4, Spot +4
> - **Languages** Giant
> - **Base Attack/Grapple** +3/+6
> - **Attack** Claw +7 to hit (1d6 + 2)
> - **Full Attack** Two Claws Claw +7 to hit (1d6 + 2)
> - **Space/Reach** 5 ft./5 ft.
> - **Special Attacks** -
> - **Special Qualities** Darkvision 60 ft.
> - **Saves** Fort +6, Ref +4, Will +1
> - **Languages** Giant

___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|16 (+3)|14 (+2)|12 (+1)|15 (+3)|10 (+0)|6 (-2)|

> - **Challenge Rating** 2

___

<img src='https://vignette.wikia.nocookie.net/witcher/images/2/25/Nekker_Warrior.png/revision/latest?cb=20160515191705' />

<div class='pageNumber'>1</div>
<div class='footnote'>WITCHER 3 | MONSTERS</div>
